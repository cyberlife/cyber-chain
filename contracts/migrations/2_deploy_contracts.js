var WorkToken = artifacts.require('contracts/token/WorkToken.sol');
var Staking = artifacts.require('contracts/security/Staking.sol');
var HostManagement = artifacts.require('contracts/hosts/HostManagement.sol');
var AgentObligations = artifacts.require('contracts/agents/AgentObligations.sol');
var AgentManagement = artifacts.require('contracts/agents/AgentManagement.sol');
var AgentsTreasury = artifacts.require('contracts/agents/AgentsTreasury.sol');
var DeploymentRequests = artifacts.require('contracts/agents/DeploymentRequests.sol');
var ShardModifications = artifacts.require('contracts/agents/ShardModifications.sol');
var AgentVersioning = artifacts.require('contracts/hosts/AgentVersioning.sol');
var AssigningJobs = artifacts.require('contracts/hosts/AssigningJobs.sol');
var EphemeralPermissions = artifacts.require('contracts/agents/EphemeralPermissions.sol');

var Web3 = require('web3')

module.exports = async function (deployer, network, accounts) {

    let STAKE = '200000000000000000000'
    let POOL_WITHDRAWAL_TIME = 7
    let toMint = '1000000000000000000000000'
    let toApprove = '1000000000000000000000'
    let rent1 = 11
    let rent2 = 10
    let rent3 = 9
    let shardsBatchSize = 2
    let shardVoteThreshold = 1
    let paymentWindow = 60000
    let votingWindow = 60
    let timeBetweenPriceChanges = 64000
    let shutdownTime = 60
    let keyActivity = 30
    let minimumExecutionTime = 30

    await unlockAllAccounts();

    console.log("Unlocked all accounts!");

    return deployer.deploy(WorkToken, "WORK", "WOR", 18, {from: accounts[0]}).then(function() {
    return deployer.deploy(Staking, WorkToken.address, STAKE, {from: accounts[0]}).then(function() {
    return deployer.deploy(AgentManagement, 1, {from: accounts[0]}).then(function() {
    return deployer.deploy(AgentObligations, AgentManagement.address, {from: accounts[0]}).then(function() {
    return deployer.deploy(AgentsTreasury, paymentWindow, {from: accounts[0]}).then(function() {
    return deployer.deploy(HostManagement, Staking.address, AgentsTreasury.address, {from: accounts[0]}).then(function() {
    return deployer.deploy(ShardModifications, shardsBatchSize, shardVoteThreshold, votingWindow, {from: accounts[0]}).then(function() {
    return deployer.deploy(DeploymentRequests, {from: accounts[0]}).then(function() {
    return deployer.deploy(AgentVersioning, AgentManagement.address, Staking.address, {from: accounts[0]}).then(function() {
    return deployer.deploy(AssigningJobs, HostManagement.address, AgentManagement.address, AgentsTreasury.address, {from: accounts[0]}).then(function() {
    return deployer.deploy(EphemeralPermissions, keyActivity, HostManagement.address, {from: accounts[0]}).then(async function() {

    var workToken = await WorkToken.deployed();
    var staking = await Staking.deployed();
    var agentManagement = await AgentManagement.deployed();
    var agentObligations = await AgentObligations.deployed();
    var treasury = await AgentsTreasury.deployed();
    var hostManagement = await HostManagement.deployed();
    var shard = await ShardModifications.deployed();
    var deployments = await DeploymentRequests.deployed();
    var versioning = await AgentVersioning.deployed();
    var jobs = await AssigningJobs.deployed();
    var ephemeral = await EphemeralPermissions.deployed();

    //Mint work coins

    await workToken.mint(accounts[0], toMint, {from: accounts[0]});

    await workToken.mint(accounts[1], toMint, {from: accounts[0]});

    await workToken.mint(accounts[2], toMint, {from: accounts[0]});

    await workToken.mint(accounts[3], toMint, {from: accounts[0]});

    //Approve work coins spending

    await workToken.approve(staking.address, toApprove, {from: accounts[1]});

    await workToken.approve(staking.address, toApprove, {from: accounts[2]});

    await workToken.approve(staking.address, toApprove, {from: accounts[3]});

    //Hosts stake

    await staking.stake({from: accounts[1]});

    await staking.stake({from: accounts[2]});

    await staking.stake({from: accounts[3]});

    //Set treasury in HostManagement

    await hostManagement.setTreasury(treasury.address, {from: accounts[0]});

    //Set lower rent bound

    await hostManagement.setLowerBound(rent3, {from: accounts[0]});

    //Set time between changes and shutdown time in HostManagement

    await hostManagement.changeTimeBetweenChanges(timeBetweenPriceChanges, {from: accounts[0]});

    await hostManagement.changeMinimumShutdownTime(shutdownTime, {from: accounts[0]});

    //Set rent

    await hostManagement.changeRent(rent1, {from: accounts[1]});

    await hostManagement.changeRent(rent2, {from: accounts[2]});

    await hostManagement.changeRent(rent3, {from: accounts[3]});

    //Set ID

    await hostManagement.setID("QmcThUufvADsvfUia6pgXcpC5xFyiWJQLXywJHw7SjKV2k", {from: accounts[1]});

    await hostManagement.setID("QmdKq27XbDW6LHPUbAx2zWQDXxdBa2Bb4txtopcaoDPLdj", {from: accounts[2]});

    await hostManagement.setID("QmVi64cSbLsAJZX1DGYe6ShaHSKWL3XVaVC9M7YfvZHH98", {from: accounts[3]});

    //Broadcast activity from hosts

    await hostManagement.broadcastActivity({from: accounts[1]});

    await hostManagement.broadcastActivity({from: accounts[2]});

    await hostManagement.broadcastActivity({from: accounts[3]});

    //DeploymentRequests setup

    await deployments.setTreasuryDeployment(treasury.address, {from: accounts[0]});

    await deployments.setShardModifications(shard.address, {from: accounts[0]});

    await deployments.changeAgentManagement(agentManagement.address, {from: accounts[0]});

    await deployments.changeStakeAddress(staking.address, {from: accounts[0]});

    //ShardModifications setup

    await shard.setTreasury(treasury.address, {from: accounts[0]});

    await shard.changeDeploymentRequestAddr(deployments.address, {from: accounts[0]});

    await shard.changeAgentManagement(agentManagement.address, {from: accounts[0]});

    //AgentsTreasury setup

    await treasury.setShardModifications(shard.address, {from: accounts[0]});

    await treasury.changeDeploymentRequestAddr(deployments.address, {from: accounts[0]});

    await treasury.changeAgentManagement(agentManagement.address, {from: accounts[0]});

    await treasury.setHostManagement(hostManagement.address, {from: accounts[0]});

    //AgentManagement setup

    await agentManagement.setTreasury(treasury.address, {from: accounts[0]});

    await agentManagement.setEphemeral(ephemeral.address, {from: accounts[0]});

  })  })  })  })  })  })  }) }) }) }) })

};

async function unlockAllAccounts() {

  var masterWeb3 = new Web3(new Web3.providers.HttpProvider("http://ethermaster:8545"));

  await masterWeb3.eth.personal.unlockAccount("0xed9d02e382b34818e88B88a309c7fe71E65f419d", "", 600000)
  await masterWeb3.eth.personal.unlockAccount("0xca843569e3427144cead5e4d5999a3d0ccf92b8e", "", 600000)
  await masterWeb3.eth.personal.unlockAccount("0x0fbdc686b912d7722dc86510934589e0aaf3b55a", "", 600000)
  await masterWeb3.eth.personal.unlockAccount("0x9186eb3d20cbd1f5f992a950d808c4495153abd5", "", 600000)
  await masterWeb3.eth.personal.unlockAccount("0x0638e1574728b6d862dd5d3a3e0942c3be47d996", "", 600000)

}
