pragma solidity 0.5.0;

import "contracts/zeppelin/ERC20/MintableToken.sol";

//NEW: CHANGED CONTRACT NAME

contract WorkToken is MintableToken{

    address creator;

    string name;
    string symbol;

    uint256 decimals;

    constructor(string memory _name, string memory _symbol, uint256 _decimals) public {

        name = _name;
        symbol = _symbol;
        decimals = _decimals;

    }

    function getName() public view returns (string memory) {

        return name;

    }

    function getSymbol() public view returns (string memory) {

        return symbol;

    }

    function getDecimals() public view returns (uint256) {

        return decimals;

    }

}
