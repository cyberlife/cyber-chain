pragma solidity 0.5.0;

import "contracts/token/WorkToken.sol";
import "contracts/zeppelin/SafeMath.sol";
import "contracts/interfaces/IStaking.sol";
import "contracts/interfaces/IHostManagement.sol";

contract Staking is IStaking {

  //NEW: deleted get probability, changeProbability, changeMax stake

    using SafeMath for uint256;

    uint256 public MINIMUM_STAKE;
    uint256 public WAITING_TO_START = 3 days;
    uint256 public WAITING_TO_WITHDRAW = 12 weeks;
    uint256 public MINIMUM_TIME_TILL_WITHDRAW = 1 days;

    uint256 public stakersAmount;
    uint256 public totalStake;

    address public mainCoin;

    mapping(address => bool) isCurrentlyStaked;

    mapping(address => uint256) amountStaked;

    mapping(address => uint256) startTime;

    mapping(address => uint256) allowedWithdrawalTime;

    //NEW: deleted max probability, pools

    constructor(address _mainCoin, uint256 minimumStake) public {

        require(_mainCoin != address(0), "The mainCoin param must not be null");
        require(minimumStake > 0, "minimumStake must be greater than zero");

        mainCoin = _mainCoin;

        MINIMUM_STAKE = minimumStake;

    }

    function changeMinimumStake(uint256 newMinimumStake) public onlyOwner {

        require(newMinimumStake > 0, "The minimum stake needs to be greater than zero");

        MINIMUM_STAKE = newMinimumStake;

        emit ChangedMinimumStake(newMinimumStake);

    }

    function stake() public {

        WorkToken token = WorkToken(mainCoin);

        require(isCurrentlyStaked[msg.sender] == false, "The msg.sender must not be already staked");
        require(token.allowance(msg.sender, address(this)) >= MINIMUM_STAKE, "The msg.sender must allow the staking contract to take coins");

        token.transferFrom(msg.sender, address(this), MINIMUM_STAKE);

        isCurrentlyStaked[msg.sender] = true;

        startTime[msg.sender] = block.timestamp;

	      startTime[msg.sender] = startTime[msg.sender].add(WAITING_TO_START);

        amountStaked[msg.sender] = amountStaked[msg.sender].add(MINIMUM_STAKE);

        stakersAmount = stakersAmount.add(1);

        totalStake = totalStake.add(MINIMUM_STAKE);

        emit Staked(msg.sender, now, startTime[msg.sender], amountStaked[msg.sender]);

    }

    //NEW

    function replenishStake() public {

        WorkToken token = WorkToken(mainCoin);

        require(isCurrentlyStaked[msg.sender] == true, "The msg.sender must already be staked");
        require(amountStaked[msg.sender] < MINIMUM_STAKE, "The msg.sender must have been slashed");

        uint256 remainingStake = MINIMUM_STAKE - amountStaked[msg.sender];

        require(token.allowance(msg.sender, address(this)) >= remainingStake, "The msg.sender must allow this contract to take coins from their balance");

        token.transferFrom(msg.sender, address(this), remainingStake);

        amountStaked[msg.sender] = amountStaked[msg.sender].add(remainingStake);

        totalStake = totalStake.add(remainingStake);

        emit ReplenishedStake(msg.sender, remainingStake);

    }

    function signalWithdraw() public {

        require(isCurrentlyStaked[msg.sender] == true, "The msg.sender must already be staked");

        allowedWithdrawalTime[msg.sender] = block.timestamp.add(WAITING_TO_WITHDRAW);

        emit SignalUnstake(msg.sender, now, allowedWithdrawalTime[msg.sender]);

    }

    function withdraw() public {

        require(allowedWithdrawalTime[msg.sender] < now && allowedWithdrawalTime[msg.sender] > 0,
            "You must wait until allowedWithdrawalTime[msg.sender] to withdraw");
        require(isCurrentlyStaked[msg.sender] == true, "The msg.sender must already be staked");

        isCurrentlyStaked[msg.sender] = false;

        uint256 auxStake = amountStaked[msg.sender];

        amountStaked[msg.sender] = 0;

        totalStake = totalStake.sub(auxStake);

        stakersAmount = stakersAmount.sub(1);

        allowedWithdrawalTime[msg.sender] = 0;

        startTime[msg.sender] = 0;

        WorkToken token = WorkToken(mainCoin);

        token.transfer(msg.sender, auxStake);

        emit Unstake(msg.sender, now, auxStake);

    }

    function slash(bytes32 reason, address host, uint256 amount) public {

        require(host != address(0), "The host param must not be null");

        require(amount > 0, "The amount param must be greater than zero");

        require(reason != bytes32(''), "The reason must not be null");

        //TODO: Check reason
        //TODO: send stake to a commons, charity or essential dApp

        amountStaked[host] = amountStaked[host].sub(amount);

        totalStake = totalStake.sub(amount);

        emit Slashed(reason, host, amount);

    }

    //GETTERS

    function getStartTime(address staker) public view returns (uint256) {

        return startTime[staker];

    }

    function getIsCurrentlyStaked(address staker) public view returns (bool) {

        return isCurrentlyStaked[staker];

    }

    function getAmountStaked(address staker) public view returns (uint256) {

        return amountStaked[staker];

    }

    function getAcceptanceTillWithdraw() public view returns (uint256) {

        return MINIMUM_TIME_TILL_WITHDRAW;

    }

    function getWithdrawalTime(address staker) public view returns (uint256) {

        return allowedWithdrawalTime[staker];

    }

    function getStakeNeeded() public view returns (uint256) {

        return MINIMUM_STAKE;

    }

    function getTotalStake() public view returns (uint256) {

        return totalStake;

    }

}
