pragma solidity 0.5.0;

import "contracts/zeppelin/ownership/Ownable.sol";

contract IHostManagement is Ownable {

    event ChangedRent(address host, uint256 newRent);

    event SetID(address host, bytes32 id);

    event SetLowerRentBound(uint256 bound);

    event BroadcastedActivity(address host);

    event UpdatedAgentsRoot(address indexed host, bytes32 root, string ipfs);

    event BroadcastedShutdown(address host, uint256 shutdownTimestamp);

    event ChangedTimeBetweenChanged(uint256 newTime);

    event ChangedMinimumShutdownTime(uint256 newTime);

    event Shutdown(address host);

    event ChangedAgentsTree(address indexed host, bytes32 root, uint256 agentsNr);

    event RegisteredSeconds(address indexed host, string agent, uint256 secNr);


    function setTreasury(address _treasury) public;

    function setLowerBound(uint256 _lowerBound) public;

    function changeTimeBetweenChanges(uint256 newTime) public;

    function changeMinimumShutdownTime(uint256 newTime) public;

    function changeRent(uint256 newPrice) public;

    function setID(string memory ID) public;

    function setAgentsTree(address host, string memory ipfs, bytes32 root, uint256 agentsNumber) public;

    function broadcastActivity() public;

    function shutdown() public;

    function registerSeconds(address host, string memory agent, uint256 secNumber) public;

    function stringToBytes32(string memory source) public view returns (bytes32);


    function canHostAgents(bytes32 id) public view returns (bool);

    function stringCanHostAgents(string memory id) public view returns (bool);

    function hostPrice(string memory id) public view returns (uint256);

    function priceFromAddress(address id) public view returns (uint256);

    function hostIDfromAddr(address host) public view returns (bytes32);

    function hostAddressfromID(string memory id) public view returns (address);

    function getAgentsNumber(string memory id) public view returns (uint256);

    function getAgentsTree(address host) public view returns (string memory, bytes32);

    function getTimeBetweenChanges() public view returns (uint256);

    function getMinimumShutdownTime() public view returns (uint256);

    function getShutdownPermission(address host) public view returns (uint256);

    function hostIsActive(address host) public view returns (bool);

    function getAgentsRoot(address host) public view returns (bytes32);

    function getSecondsHosted(address host, string memory agent) public view returns (uint256);

    function getStartOfWork(address host) public view returns (uint256);

    function compareStrings(string memory a, string memory b) public view returns (bool);

}
