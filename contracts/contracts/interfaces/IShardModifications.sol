pragma solidity 0.5.0;

contract IShardModifications {

    event ChangedVotingWindow(uint256 window);

    event ChangedBatchSize(uint256 batch);

    event ChangedAgentManagement(address management);

    event ChangedDeploymentRequests(address deployments);

    event ChangedVotingThreshold(uint256 threshold);

    event ProposedModification(string name, string ipfs);

    event VotedModification(string name, bool vote);


    function changeVotingWindow(uint256 _window) public;

    function changeBatchSize(uint256 _batch) public;

    function changeVoteThreshold(uint256 _threshold) public;

    function proposeModification(string memory _name, address[] memory hosts,
        uint256[] memory timeToHost,
        uint256[] memory currentPrices,
        bytes memory root, string memory ipfs, uint256[] memory packagedUint) public;

    function applyModification(string memory _name) public payable;

    function voteModification(string memory _name, bool yesno) public;

    function getVoteThreshold() public view returns (uint256);

    function getBatchSize() public view returns (uint256);

    function getModificationTree(string memory name)
      public view returns (bytes memory, string memory);

    function getModificationArrays(string memory name)
      public view returns (address[] memory, uint256[] memory, uint256[] memory);

    function getVotingWindow() public view returns (uint256);

    function getModificationIntData(string memory _name)
      public view returns (uint256, uint256, uint256, uint256, uint256, uint256);

    function agentVoted(address agent, uint256 _nonce) public view returns (bool);

}
