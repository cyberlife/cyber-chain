pragma solidity 0.5.0;

contract IAgentVersioning {

  event ChangedAgentVersion(address host, string agent);

  event ClearedAgent(address host, string agent);

  function setManagement(address _management) public;

  function setStaking(address _staking) public;

  function updateAgentVersion(string memory _agent) public;

  function clearAgent(string memory _agent) public;

  function getHostAgentVersion(address host, string memory agent) public view returns (uint256);

}
