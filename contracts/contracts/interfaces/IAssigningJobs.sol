pragma solidity 0.5.0;

contract IAssigningJobs {

  function setHostManagement(address _management) public;

  function setAgentManagement(address _management) public;

  function setTreasury(address _treasury) public;

  function changingAgentTree(string memory agent) public;

  function finishedChangingTree(string memory agent) public;


  function getChangingTree(string memory agent) public view returns (bool);

  function getIsChangingTree(address host) public view returns (bool);

  function getHostChangingTree(string memory agent) public view returns (address);

}
