pragma solidity 0.5.0;

import "contracts/zeppelin/ownership/Ownable.sol";

contract IStaking is Ownable {

    event Staked(address staker, uint256 timestamp, uint256 startTimestamp, uint256 howMuch);

    event SignalUnstake(address staker, uint256 timestamp, uint256 whenToUnstake);

    event Unstake(address staker, uint256 timestamp, uint256 amount);

    event ChangedMinimumStake(uint256 newStake);

    event ReplenishedStake(address staker, uint256 stake);

    event Slashed(bytes32 reason, address host, uint256 amount);


    function changeMinimumStake(uint256 newMinimumStake) public;

    function stake() public;

    function signalWithdraw() public;

    function withdraw() public;

    function replenishStake() public;

    function slash(bytes32 reason, address host, uint256 amount) public;



    function getStartTime(address staker) public view returns (uint256);

    function getAcceptanceTillWithdraw() public view returns (uint256);

    function getIsCurrentlyStaked(address staker) public view returns (bool);

    function getAmountStaked(address staker) public view returns (uint256);

    function getWithdrawalTime(address staker) public view returns (uint256);

    function getStakeNeeded() public view returns (uint256);

    function getTotalStake() public view returns (uint256);

}
