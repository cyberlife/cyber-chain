pragma solidity 0.5.0;

contract IEphemeralPermissions {

  event ToggledContract(address con, bool status);

  event ChangedAvailability(uint256 availability);

  event ChangedHostManagement(address host);

  event ProposedPermission(string agent, bytes32 hashOfParams, bytes trustedExecutionProof);

  event UsedPermission(string agent, uint256 position);


  function toggleAllowedContract(address con) public;

  function changeAvailability(uint256 _availability) public;

  function changeHostManagement(address _host) public;

  function proposePermission(string memory agent, bytes32 hashOfParams,
                             bytes memory trustedExecutionProof,
                             address _forContract, string memory _func,
                             bytes memory hostingProof) public;

  function usePermission(string memory agent, uint256 position) public;


  //PRIVATE

  function isContract(address _addr) private returns (bool);


  function canUsePermission(string memory agent, uint256 position,
    address con, string memory func, address caller, bytes32 paramsHash)
    public view returns (bool);

  function getContractWhereUsed(string memory agent, uint256 position)
    public view returns (address);

  function getLatestNonce(string memory agent) public view returns (uint256);

  function getHashOfParams(string memory agent, uint256 position)
    public view returns (bytes32);

  function getTrustedExecutionProof(string memory agent, uint256 position)
    public view returns (bytes memory);

  function getFunctionWhereUsed(string memory agent, uint256 position)
    public view returns (string memory);

  function getCreationTime(string memory agent, uint256 position)
    public view returns (uint256);

  function getUsed(string memory agent, uint256 position) public view returns (bool);

  function getHost(string memory agent, uint256 position) public view returns (address);

  function getAvailability() public view returns (uint256);

  function getContractAllowance(address con) public view returns (bool);

}
