pragma solidity 0.5.0;

contract IAgentManagement {

    event CreatedNewAgent(address creator, uint256 positionInArray);

    event MadeAutonomous(address creator, uint256 position);

    event UpdatedCode(address caller, address creator, uint256 position, string codeLocation, bytes32 codeHash);

    event ChangedAgentContract(address caller, address creator, uint256 position, address newContract);

    event ChangedShardSize(uint256 newSize);


    function setEphemeral(address _ephemeral) public;

    function setTreasury(address _treasury) public;

    function changeMinimumShardSize(uint256 newShardSize) public;

    function newAgent(bool agentIsCreator,
        string memory _agent,
        uint256 position,
        string memory _name,
        string memory open_source,
        bytes32 code_hash,
        address newBotAddress) public;

    function deleteAgent(string memory _name, uint256 position) public;

    function makeFullyAutonomous(string memory _name, uint256 position) public;

    function updateCode(string memory _name, uint256 position,
      string memory codeSource, bytes32 codeHash) public;


    function getMinimumShardSize() public view returns (uint256);

    function getAgentControllingAgent(string memory _controlledAgent)
      public view returns (string memory);

    function getCreatorIsAgent(address creator, uint256 position)
      public view returns (bool);

    function getCreatorFromName(string memory name) public view returns (address);

    function getPositionFromName(string memory name) public view returns (uint256);

    function getAgentName(address creator, uint256 position)
      public view returns (string memory);

    function getAgentVersion(address creator, uint256 position)
      public view returns (uint256);

    function getAgentSourceLocation(address creator, uint256 position)
      public view returns (string memory);

    function getCurrentController(address creator, uint256 position)
      public view returns (address);

    function getAgentAddress(address creator, uint256 position)
      public view returns (address);

    function getAgentCodeHash(address creator, uint256 position)
      public view returns (bytes32);

    function getAgentAutonomy(address creator, uint256 position)
      public view returns (bool);

    function isNameTaken(string memory _name) public view returns (bool);

}
