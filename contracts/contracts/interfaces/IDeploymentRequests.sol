pragma solidity 0.5.0;

contract IDeploymentRequests {

    event ChangedStakingAddress(address staking);

    event ChangedMerkleUtils(address utils);

    event DeployedAgent(string name, string ipfs, string billing, uint256 responsesNr);

    event Responded(uint256 totalResponses, bool readyToApply);

    event ChangedAgentManagement(address management);

    event WithdrewAgent(string _name, bytes _root, string _ipfs, uint256 totalHostsNr);


    function setTreasuryDeployment(address _treasury) public;

    function setShardModifications(address _shardModifications) public;

    function changeStakeAddress(address _staking) public;

    function changeAgentManagement(address _newManagement) public;

    function deployAgent(string memory _name, bytes memory _hostsRoot,
        string memory _ipfs,
        bytes memory billingRoot, string memory billingProof,
        uint256 responsesNumber, uint256 moneyToDeposit) public;

    function withdrawAgent(string memory _name, bytes memory _root,
      string memory _ipfs, uint256 totalHostsNr) public;

    function respond(string memory _name, uint256 position, bytes memory _proof) public;

    function hasEnoughResponses(string memory _name, uint256 position)
      public view returns (bool);

    function getDeploymentsLength(string memory _name) public view returns (uint256);

    function getDeploymentIPFSProof(string memory _name, uint256 position)
      public view returns (string memory);

    function getDeploymentHostsRoot(string memory _name, uint256 position)
      public view returns (bytes memory);

    function getDeploymentBillingRoot(string memory _name, uint256 position)
      public view returns (bytes memory);

    function getDeploymentBillingProof(string memory _name, uint256 position)
      public view returns (string memory);

    function getWithdrawalsLength(string memory _name) public view returns (uint256);

    function getWithdrawalRoot(string memory _name, uint256 position)
      public view returns (bytes memory);

    function getWithdrawalIPFSProof(string memory _name, uint256 position)
      public view returns (string memory);

    function getDeploymentIntData(string memory _name, uint256 position)
      public view returns (uint256[] memory);

    function getDeploymentHostsTree(string memory _name, uint256 position)
      public view returns (bytes memory, string memory);

    function getWithdrawalData(string memory _name, uint256 position)
      public view returns (bytes memory, string memory, uint256);

}
