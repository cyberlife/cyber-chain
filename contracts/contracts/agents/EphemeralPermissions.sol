pragma solidity 0.5.0;

import "contracts/interfaces/IEphemeralPermissions.sol";
import "contracts/interfaces/IHostManagement.sol";
import "contracts/zeppelin/ownership/Ownable.sol";

import "contracts/zeppelin/SafeMath.sol";

contract EphemeralPermissions is IEphemeralPermissions, Ownable {

  using SafeMath for uint256;

  struct Permission {

    address host;

    bytes32 hashOfParams;

    bytes trustedExecutionProof;

    address contractWhereUsed;

    string functionWhereUsed;

    uint256 creationTime;

    bool used;

  }

  uint256 availability;

  IHostManagement hostManagement;

  mapping(string => mapping(uint256 => Permission)) permissions;

  mapping(string => uint256) latestNonce;

  mapping(address => bool) allowedUsageLocations;


  constructor(uint256 _availability, address _hostManagement) public {

    require(_availability > 0);
    require(_hostManagement != address(0));

    availability = _availability;

    hostManagement = IHostManagement(_hostManagement);

  }

  function toggleAllowedContract(address con) public onlyOwner {

    require(con != address(0), "The con address is null");

    allowedUsageLocations[con] = !allowedUsageLocations[con];

    emit ToggledContract(con, allowedUsageLocations[con]);

  }

  function changeAvailability(uint256 _availability) public onlyOwner {

    require(_availability > 0, "Availability must be greater than zero");

    availability = _availability;

    emit ChangedAvailability(_availability);

  }

  function changeHostManagement(address _host) public onlyOwner {

    require(_host != address(0), "The host management param is null");

    hostManagement = IHostManagement(_host);

    emit ChangedHostManagement(_host);

  }

  function proposePermission(string memory agent, bytes32 hashOfParams,
                             bytes memory trustedExecutionProof,
                             address _forContract, string memory _func,
                             bytes memory hostingProof) public {

    //[PRODUCTION] check if a host having the agent is updating this
    //[PRODUCTION] punish host if they don't use the key in the allocated availability seconds

    require(allowedUsageLocations[_forContract] == true, "forContract is not allowed");

    bytes32 hostId = hostManagement.hostIDfromAddr(msg.sender);

    require(hostManagement.canHostAgents(hostId) == true, "The msg.sender is not able to interact right now");

    Permission memory agentPermission =
      Permission(msg.sender, hashOfParams, trustedExecutionProof, _forContract, _func, now, false);

    permissions[agent][latestNonce[agent]] = agentPermission;

    latestNonce[agent] = latestNonce[agent].add(1);

    emit ProposedPermission(agent, hashOfParams, trustedExecutionProof);

  }

  function usePermission(string memory agent, uint256 position) public {

    require(permissions[agent][position].contractWhereUsed == msg.sender, "This is not the contract where the permission needs to be used");

    require(allowedUsageLocations[msg.sender] == true, "This contract is not a valid usage location");

    require(permissions[agent][position].creationTime + availability >= now, "The availability expired");

    require(permissions[agent][position].used == false, "The permission is already used");

    permissions[agent][position].used = true;

    emit UsedPermission(agent, position);

  }

  //PRIVATE

  function isContract(address _addr) private returns (bool) {

    uint32 size;

    assembly {
      size := extcodesize(_addr)
    }

    return (size > 0);

  }

  //GETTERS

  function canUsePermission(string memory agent, uint256 position, address con,
    string memory func, address caller, bytes32 paramsHash) public view returns (bool) {

    bytes32 hostId = hostManagement.hostIDfromAddr(caller);

    return(

      permissions[agent][position].used == false &&
      permissions[agent][position].host == caller &&
      hostManagement.canHostAgents(hostId) == true &&
      keccak256(abi.encodePacked(permissions[agent][position].functionWhereUsed)) ==
      keccak256(abi.encodePacked(func)) &&
      permissions[agent][position].creationTime + availability > now &&
      permissions[agent][position].hashOfParams == paramsHash

    );

  }

  function getLatestNonce(string memory agent) public view returns (uint256) {

    return latestNonce[agent];

  }

  function getContractWhereUsed(string memory agent, uint256 position)
    public view returns (address) {

    return permissions[agent][position].contractWhereUsed;

  }

  function getFunctionWhereUsed(string memory agent, uint256 position)
    public view returns (string memory) {

    return permissions[agent][position].functionWhereUsed;

  }

  function getHashOfParams(string memory agent, uint256 position)
    public view returns (bytes32) {

    return permissions[agent][position].hashOfParams;

  }

  function getTrustedExecutionProof(string memory agent, uint256 position)
    public view returns (bytes memory) {

    return permissions[agent][position].trustedExecutionProof;

  }

  function getCreationTime(string memory agent, uint256 position)
    public view returns (uint256) {

    return permissions[agent][position].creationTime;

  }

  function getUsed(string memory agent, uint256 position) public view returns (bool) {

    return permissions[agent][position].used;

  }

  function getHost(string memory agent, uint256 position) public view returns (address) {

    return permissions[agent][position].host;

  }

  function getAvailability() public view returns (uint256) {

    return availability;

  }

  function getContractAllowance(address con) public view returns (bool) {

    return allowedUsageLocations[con];

  }

}
