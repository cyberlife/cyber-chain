pragma solidity 0.5.0;

import "contracts/zeppelin/SafeMath.sol";
import "contracts/interfaces/IAgentManagement.sol";
import "contracts/interfaces/IAgentsTreasury.sol";
import "contracts/interfaces/IDeploymentRequests.sol";
import "contracts/interfaces/IShardModifications.sol";
import "contracts/zeppelin/ownership/Ownable.sol";

contract ShardModifications is IShardModifications, Ownable {

    struct Modification {

        uint256[] intData;

        bytes newRoot;

        string ipfsProof;

        address[] hosts;

        uint256[] timeToHost;

        uint256[] currentPrices;

    }

    mapping(string => Modification) proposedModification;

    mapping(address => mapping(uint256 => bool)) voted;

    address utils;

    address[] voters;

    uint256 batchSize;
    uint256 voteThreshold;
    uint256 votingWindow;
    uint256 nonce;

    uint256[] proposalData = new uint256[](6);

    IAgentManagement management;
    IDeploymentRequests requests;
    IAgentsTreasury tr;

    constructor(uint256 maxBatchSize, uint256 modificationVoteThreshold,
                uint256 _votingWindow) public {

        require(maxBatchSize > 0, "Batch size must be > than 0");
        require(_votingWindow > 0, "Voting window must be > than 0");

        batchSize = maxBatchSize;

        voteThreshold = modificationVoteThreshold;

        votingWindow = _votingWindow;

    }

    function setTreasury(address _treasury) public onlyOwner {

        require(_treasury != address(0), "The _treasury param is null");

        tr = IAgentsTreasury(_treasury);

    }

    function changeDeploymentRequestAddr(address _deploymentReq) public onlyOwner {

        require(_deploymentReq != address(0), "The deployment requests param is null");

        requests = IDeploymentRequests(_deploymentReq);

        emit ChangedDeploymentRequests(_deploymentReq);

    }

    function changeVotingWindow(uint256 _window) public onlyOwner {

        require(_window > 0, "The voting window must be greater than zero");

        votingWindow = _window;

        emit ChangedVotingWindow(_window);

    }

    function changeAgentManagement(address _newManagement) public onlyOwner {

        require(_newManagement != address(0), "The _newManagement param is null");

        management = IAgentManagement(_newManagement);

        emit ChangedAgentManagement(_newManagement);

    }

    function changeBatchSize(uint256 _batch) public onlyOwner {

        require(_batch > 0, "_batch must be greater than zero");

        batchSize = _batch;

        emit ChangedBatchSize(_batch);

    }

    function changeVoteThreshold(uint256 _threshold) public onlyOwner {

        voteThreshold = _threshold;

        emit ChangedVotingThreshold(_threshold);

    }

    function proposeModification(string memory _name, address[] memory hosts,
        uint256[] memory timeToHost, uint256[] memory currentPrices,
        bytes memory root, string memory ipfs, uint256[] memory packagedUint) public {

        require(hosts.length <= batchSize && hosts.length > 0, "Too many or too few hosts were bulked together");

        //[PRODUCTION] CHECK THAT AGENT IS DEPLOYED

        //[PRODUCTION] IF THIS IS A DEPLOYMENT, CHECK IF ALL HOSTS ACCEPTED

        bytes memory comparisonRoot;

        uint i;

        uint256 position;

        if (packagedUint[0] == 1) {

          require(hosts.length == currentPrices.length && hosts.length == timeToHost.length, "Arrays need to have the same length");

          position = requests.getWithdrawalsLength(_name);

          comparisonRoot = requests.getWithdrawalRoot(_name, position - 1);

        }

        else if (packagedUint[0] == 0) {

          position = requests.getDeploymentsLength(_name);

          comparisonRoot = requests.getDeploymentBillingRoot(_name, position - 1);

        }

        require(keccak256(comparisonRoot) == keccak256(root), "The root in deployment and the param root must be the same");

        //PRODUCTION: check if the votingPeriod has passed since the last proposed modification

        require(bytes(_name).length > 0, "You did not provide a name");
        require(root.length > 0, "Did not pass a merkle root in the root array");

        //Create array with creationTime, modifType, hostsNr, acceptedVotes, declinedVotes, moneyToBeDeposited

         proposalData[0] = now;
         proposalData[1] = packagedUint[0];
         proposalData[2] = packagedUint[1];
         proposalData[3] = 0;
         proposalData[4] = 0;
         proposalData[5] = packagedUint[2];

         delete(proposedModification[_name]);

         Modification memory newModification = Modification(proposalData, root,
                                                            ipfs, hosts,
                                                            timeToHost, currentPrices);

         proposedModification[_name] = newModification;

         nonce = nonce + 1;

         emit ProposedModification(_name, ipfs);

    }

    function voteModification(string memory _name, bool yesno) public {

        //[PRODUCTION] voters need to have staked

        require(voteThreshold > 0, "Vote threshold not specified");
        require(proposedModification[_name].intData[0] > 0, "Modification must exist");
        require(now < proposedModification[_name].intData[0] + votingWindow * 1 seconds, "You must vote within the voting window");

        require(voted[msg.sender][nonce] == false, "Address did already vote");

        voted[msg.sender][nonce] = !voted[msg.sender][nonce];

        if (yesno == true) {

            proposedModification[_name].intData[3] = proposedModification[_name].intData[3] + 1;

        } else {

            proposedModification[_name].intData[4] = proposedModification[_name].intData[4] + 1;

        }

        emit VotedModification(_name, yesno);

    }

    function applyModification(string memory _name) public payable {

        if (proposedModification[_name].intData[1] == 0)
          require(msg.value == proposedModification[_name].intData[5], "You need to deposit the right amount of money");

        if (voteThreshold > 0) require(proposedModification[_name].intData[3] >= voteThreshold, "Does not have enough votes");

        delete(proposedModification[_name].intData[3]);

        tr.applyModification.value(msg.value)
                               (_name, proposedModification[_name].intData, proposedModification[_name].newRoot,
                               proposedModification[_name].ipfsProof, proposedModification[_name].hosts,
                               proposedModification[_name].timeToHost, proposedModification[_name].currentPrices);

        delete(proposedModification[_name]);

    }

    //GETTERS

    function getBatchSize() public view returns (uint256) {

        return batchSize;

    }

    function getVoteThreshold() public view returns (uint256) {

        return voteThreshold;

    }

    function getVotingWindow() public view returns (uint256) {

        return votingWindow;

    }

    function getModificationTree(string memory name)
      public view returns (bytes memory, string memory) {

      string memory auxIPFS = proposedModification[name].ipfsProof;

      return (proposedModification[name].newRoot, auxIPFS);

    }

    function getModificationArrays(string memory name)
      public view returns (address[] memory, uint256[] memory, uint256[] memory) {

      return (proposedModification[name].hosts, proposedModification[name].timeToHost, proposedModification[name].currentPrices);

    }

    function getModificationIntData(string memory _name)
      public view returns (uint256, uint256, uint256, uint256, uint256, uint256) {

        return (proposedModification[_name].intData[0], proposedModification[_name].intData[1],
                proposedModification[_name].intData[2], proposedModification[_name].intData[3],
                proposedModification[_name].intData[4], proposedModification[_name].intData[5]);

    }

    //NEW: nonce

    function agentVoted(address agent, uint256 _nonce) public view returns (bool) {

        return voted[agent][_nonce];

    }

}
