pragma solidity 0.5.0;

import "contracts/zeppelin/SafeMath.sol";
import "contracts/interfaces/IStaking.sol";
import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IAgentManagement.sol";
import "contracts/interfaces/IDeploymentRequests.sol";
import "contracts/interfaces/IShardModifications.sol";
import "contracts/interfaces/IAgentsTreasury.sol";

contract DeploymentRequests is IDeploymentRequests, Ownable {

    using SafeMath for uint256;

    //Constants

    string DEPLOY_AGENT = "deployAgent";
    string WITHDRAW_AGENT = "withdrawAgent";

    struct Deployment {

       bytes hostsRoot;

       string ipfsProof;

       bytes billingRoot;

       string billingProof;

       uint256[] intData;

    }

    struct Withdrawal {

        bytes root;

        string ipfsProof;

        uint256 remainingHosts;

    }

    mapping(string => Deployment[]) deployments;

    mapping(string => Withdrawal[]) withdrawals;

    mapping(address => mapping(string => mapping(uint256 => bool))) responses;

    modifier onlyStakedHost() {

        IStaking s = IStaking(staking);

        require(s.getIsCurrentlyStaked(msg.sender) == true, "The sender does not have a stake");

        _;

    }

    modifier responsesDeadline(string memory _name, uint256 position) {

        require(now - deployments[_name][position].intData[1] <= 1 minutes, "Response is coming too late");
        _;

    }

    modifier noPreviousResponse(string memory _name, uint256 position) {

        require(responses[msg.sender][_name][position] == false, "Address did not already respond");
        _;

    }

    address staking;
    address utils;

    IAgentManagement management;
    IShardModifications shard;
    IAgentsTreasury tr;

    uint256[] deployData = new uint256[](4);

    constructor() public {}

    function setTreasuryDeployment(address _treasury) public onlyOwner {

        require(_treasury != address(0), "The param address is null");

        tr = IAgentsTreasury(_treasury);

    }

    function setShardModifications(address _shardModifications) public onlyOwner {

        require(_shardModifications != address(0), "The param address is null");

        shard = IShardModifications(_shardModifications);

    }

    function changeStakeAddress(address _staking) public onlyOwner {

        require(_staking != address(0), "The param address is null");

        staking = _staking;

        emit ChangedStakingAddress(_staking);

    }

    function changeAgentManagement(address _newManagement) public onlyOwner {

        require(_newManagement != address(0), "The param address is null");

        management = IAgentManagement(_newManagement);

        emit ChangedAgentManagement(_newManagement);

    }

    function deployAgent(
        string memory _name,
        bytes memory _hostsRoot,
        string memory _ipfs,
        bytes memory billingRoot,
        string memory billingProof,
        uint256 responsesNumber,
        uint256 moneyToDeposit) public {

        require(responsesNumber > 0, "You need a positive responsesNumber");

        bool isRegistered = tr.agentIsRegistered(_name);

        if (isRegistered == false) {

          require(responsesNumber >= management.getMinimumShardSize(),
            "Agent is not registered so you need to deploy to at least minimumShardSize hosts");

        } else {

          require(responsesNumber <= shard.getBatchSize(), "Agent is registered so hosts number needs to be maximum batchSize");

        }

        deployData[0] = responsesNumber;
        deployData[1] = now;
        deployData[2] = 0;
        deployData[3] = moneyToDeposit;

        Deployment memory newDeployment = Deployment(_hostsRoot, _ipfs, billingRoot, billingProof, deployData);

        deployments[_name].push(newDeployment);

        emit DeployedAgent(_name, _ipfs, billingProof, responsesNumber);

    }

    function withdrawAgent(string memory _name, bytes memory _root,
      string memory _ipfs, uint256 totalHostsNr)
      public {

        Withdrawal memory newWithdraw = Withdrawal(_root, _ipfs, totalHostsNr);

        withdrawals[_name].push(newWithdraw);

        emit WithdrewAgent(_name, _root, _ipfs, totalHostsNr);

    }

    function respond(string memory _name, uint256 position, bytes memory _proof)
      public onlyStakedHost
      responsesDeadline(_name, position) noPreviousResponse(_name, position)
      {

        //[PRODUCTION] need to check if host waited enough time after staking
        //[PRODUCTION] check that the ID of the caller is in host merkle tree

        bytes memory root = getDeploymentHostsRoot(_name, position);

        require(root.length > 0, "The root length is zero");

        require(deployments[_name][position].intData[2] < deployments[_name][position].intData[0], "Only a certain nr of hosts can vote");

        responses[msg.sender][_name][position] = true;

        deployments[_name][position].intData[2] ++;

        bool readyToApply = deployments[_name][position].intData[2] == deployments[_name][position].intData[0];

        emit Responded(deployments[_name][position].intData[2], readyToApply);

    }

    //GETTERS

    function hasEnoughResponses(string memory _name, uint256 position)
      public view returns (bool) {

        return deployments[_name][position].intData[0] ==
               deployments[_name][position].intData[2];

    }

    function getDeploymentsLength(string memory _name) public view returns (uint256) {

        return deployments[_name].length;

    }

    function getDeploymentIPFSProof(string memory _name, uint256 position)
      public view returns (string memory) {

        return deployments[_name][position].ipfsProof;

    }

    function getDeploymentHostsRoot(string memory _name, uint256 position)
      public view returns (bytes memory) {

        return deployments[_name][position].hostsRoot;

    }

    function getDeploymentBillingRoot(string memory _name, uint256 position)
      public view returns (bytes memory) {

        return deployments[_name][position].billingRoot;

    }

    function getDeploymentBillingProof(string memory _name, uint256 position)
      public view returns (string memory) {

        return deployments[_name][position].billingProof;

    }

    function getWithdrawalsLength(string memory _name) public view returns (uint256) {

        return withdrawals[_name].length;

    }

    function getWithdrawalRoot(string memory _name, uint256 position)
      public view returns (bytes memory) {

        return withdrawals[_name][position].root;

    }

    function getWithdrawalIPFSProof(string memory _name, uint256 position)
      public view returns (string memory) {

        return withdrawals[_name][position].ipfsProof;

    }

    function getDeploymentIntData(string memory _name, uint256 position)
      public view returns (uint256[] memory) {

        uint256[] memory data = new uint256[](4);

        data[0] = deployments[_name][position].intData[0];
        data[1] = deployments[_name][position].intData[1];
        data[2] = deployments[_name][position].intData[2];
        data[3] = deployments[_name][position].intData[3];

        return data;

    }

    function getDeploymentHostsTree(string memory _name, uint256 position)
      public view returns (bytes memory, string memory) {

        return (deployments[_name][position].hostsRoot, deployments[_name][position].ipfsProof);

    }

    function getWithdrawalData(string memory _name, uint256 position)
      public view returns (bytes memory, string memory, uint256) {

        return (withdrawals[_name][position].root, withdrawals[_name][position].ipfsProof,
                withdrawals[_name][position].remainingHosts);

    }

}
