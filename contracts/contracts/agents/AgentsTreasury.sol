pragma solidity 0.5.0;

import "contracts/zeppelin/SafeMath.sol";
import "contracts/interfaces/IAgentManagement.sol";
import "contracts/interfaces/IHostManagement.sol";
import "contracts/interfaces/IAgentsTreasury.sol";
import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IDeploymentRequests.sol";

contract AgentsTreasury is IAgentsTreasury, Ownable {

    using SafeMath for uint256;

    struct Root {

        uint256 timestamp;

        bytes _root;

        string ipfsProof;

        uint256 hostsNumber;

    }

    struct Agent {

        uint256[] intData;

        Root root;

    }

    mapping(string => Agent) agentsData;

    mapping(address => mapping(string => uint256)) contributions;

    mapping(address => mapping(string => bool)) withdrewFunding;

    mapping(address => mapping(string => uint256)) secondsBilled;

    mapping(address => uint256) earnedFunds;

    IAgentManagement agentManage;
    IHostManagement hostManage;
    IDeploymentRequests requests;

    address utils;

    address shardModifications;

    uint256 paymentWindow;

    uint256[] registerArray = new uint256[](4);

    modifier onlyShardModifications() {

        require(msg.sender == shardModifications, "The caller is not the shard modifications contract");

        _;

    }

    modifier onlyAgentsManagement() {

        require(msg.sender == address(agentManage), "The caller is not the agent management contract");

        _;

    }

    constructor(uint256 _paymentWindow) public {

        require(_paymentWindow > 0, "The payment window must be greater than zero");

        paymentWindow = _paymentWindow;

    }

    function setShardModifications(address _shardModifications) public onlyOwner {

        require(_shardModifications != address(0), "The specified shard modifications address must be different than 0x0");

        shardModifications = _shardModifications;

        emit ChangedShardModifications(_shardModifications);

    }

    function setHostManagement(address hostM) public onlyOwner {

        require(hostM != address(0), "The specified host management address must be different than 0x0");

        hostManage = IHostManagement(hostM);

        emit SetHostManagement(hostM);

    }

    function changeDeploymentRequestAddr(address _deploymentReq) public onlyOwner {

        require(_deploymentReq != address(0), "The specified deployment requests address must be different than 0x0");

        requests = IDeploymentRequests(_deploymentReq);

        emit ChangedDeploymentRequests(_deploymentReq);

    }

    function changePaymentWindow(uint256 window) public onlyOwner {

        require(window > 0, "The payment window must be greater than zero");

        paymentWindow = window;

        emit ChangedPaymentWindow(window);

    }

    function changeAgentManagement(address _newManagement) public onlyOwner {

        require(_newManagement != address(0), "The specified agent management address must be different than 0x0");

        agentManage = IAgentManagement(_newManagement);

        emit ChangedAgentManagement(_newManagement);

    }

    function registerAgent(string memory _name, bytes memory first_root, string memory ipfs,
      uint256 hostsNumber)
      public payable {

        uint256 deploymentsNumber = requests.getDeploymentsLength(_name) - 1;

        require(requests.getDeploymentIntData(_name, deploymentsNumber)[3] == msg.value, "Did not send enough money");

        require(agentManage.getMinimumShardSize() <= hostsNumber, "Agent not installed on enough hosts");

        uint256 position = requests.getDeploymentsLength(_name);

        require(agentsData[_name].root.timestamp == 0, "The agent with this name is already initialized");

        require(requests.hasEnoughResponses(_name, position - 1) == true, "Agent does not have enough responses from hosts");

        bytes memory comparisonRoot = requests.getDeploymentBillingRoot(_name, position - 1);

        require(keccak256(abi.encodePacked(comparisonRoot)) ==
          keccak256(abi.encodePacked(first_root)), "Roots need to be the same");

        Root memory newRoot = Root(now, first_root, ipfs, hostsNumber);

        //Create uint array with totalFunds, currentFunds, creationTime, unregisterTime

        registerArray[0] = msg.value;
        registerArray[1] = msg.value;
        registerArray[2] = now;
        registerArray[3] = 0;

        Agent memory newAgent = Agent(registerArray, newRoot);

        agentsData[_name] = newAgent;

        emit RegisteredAgent(_name, first_root, ipfs, hostsNumber);

    }

    function unregisterAgent(string memory _name) public onlyAgentsManagement {

        if (agentsData[_name].intData[2] != 0) {

          agentsData[_name].intData[3] = now;

          emit UnregisteredAgent(_name);

        }

    }

    function applyModification(string memory _name, uint256[] memory intData, bytes memory _newRoot,
                               string memory ipfsProof, address[] memory hosts,
                               uint256[] memory secondsToHost, uint256[] memory currentPrices)
                               public payable onlyShardModifications {

        if (intData[0] == 0)
          require(msg.value > 0, "You did not provide money to be deposited");

        else if (intData[0] == 1)
          require(msg.value == 0, "You provided money for a withdrawal. Call this method with msg.value = 0");

        require(agentsData[_name].root.timestamp > 0, "Agent needs to be registered in order to be modified");
        require(agentsData[_name].intData[3] == 0, "Agent was unregistered, cannot modify");

        if (intData[0] == 1) {

            require( agentManage.getMinimumShardSize() <= agentsData[_name].root.hostsNumber.sub(hosts.length) , "You are withdrawing from too many hosts");

        }

        Root memory newRoot = Root(now, _newRoot,
                                   ipfsProof, intData[2]);

        agentsData[_name].root = newRoot;

        uint256 referenceTime = now;
        uint256 i;
        uint256 toBill = 0;
        address payable currentHost;

        if (intData[1] == 1) {

          for (i = 0; i < hosts.length; i++) {

              toBill = getSecondsToBill(hosts[i], _name, secondsToHost[i]);

              if (toBill > 0 && currentHost != address(0)) {

                  secondsBilled[hosts[i]][_name] =
                    secondsBilled[hosts[i]][_name] + toBill;

                  uint256 toPay = toBill * currentPrices[i];

                  agentsData[_name].intData[1] =
                    agentsData[_name].intData[1].sub(toPay);

                  currentHost = address(uint160(hosts[i]));

                  currentHost.transfer(toPay);

                  emit Transfer(currentHost, toPay);

              }

          }

        }

        emit AppliedModification(_name, _newRoot, ipfsProof);

    }

    function fund(string memory _name) public payable {

        require(msg.value > 0, "You must specify a positive number for the msg.value");
        require(agentsData[_name].intData[3] == 0, "agentsData[_name].intData[3] is not zero");
        require(agentsData[_name].intData[2] > 0, "agentsData[_name].intData[2] is zero");

        agentsData[_name].intData[1] = agentsData[_name].intData[1].add(msg.value);
        contributions[msg.sender][_name] = contributions[msg.sender][_name].add(msg.value);
        agentsData[_name].intData[0] = agentsData[_name].intData[0].add(msg.value);

        emit Funded(msg.sender, _name);

    }

    //NEW

    function addEarnedFunds(address toAgent) public payable {

        if (toAgent != address(0)) {

          earnedFunds[toAgent] = earnedFunds[toAgent].add(msg.value);

          emit EarnedMoney(toAgent, msg.value);

        } else {

          earnedFunds[msg.sender] = earnedFunds[msg.sender].add(msg.value);

          emit EarnedMoney(msg.sender, msg.value);

        }

    }

    function withdrawFunds(string memory _name)
      public returns (uint256, uint256, uint256) {

        require(contributions[msg.sender][_name] > 0, "You did not contribute to this agent");

        require(agentsData[_name].intData[1] > 0, "Agent needs to have remaining money");

        require(withdrewFunding[msg.sender][_name] == false, "Address already withdrew funds");

        require(agentsData[_name].intData[2] > 0, "Agent needs to be registered");

        require(agentsData[_name].intData[3] > 0, "Agent needs to be unregistered");

        require(agentsData[_name].intData[3] + paymentWindow * 1 seconds < now, "You can withdraw after hosts get paid");

        withdrewFunding[msg.sender][_name] = true;

        uint256 percentage = contributions[msg.sender][_name] * 100 * 10**18 / agentsData[_name].intData[0];

        uint256 toBill = agentsData[_name].intData[1] * percentage / 100 / 10**18;

        agentsData[_name].intData[1] = agentsData[_name].intData[1].sub(toBill);

        msg.sender.transfer(toBill);

        emit WithdrewFunds(msg.sender, _name);

        return (percentage, toBill, agentsData[_name].intData[1]);

    }

    function getPaid(string memory _name, bytes memory _proof,
                     uint256 timeStartedHostingAgent, uint256 secondsToHost,
                     uint256 pricePerSecond)
                     public returns (uint256, uint256, uint256, uint256) {

        //[PRODUCTION] check with merkle tree that caller can get paid

        require(agentsData[_name].intData[1] > 0, "agentsData[_name].intData[1] is zero");

        if (agentsData[_name].intData[3] > 0)
            require(agentsData[_name].intData[3].add(paymentWindow * 1 seconds) > now, "The withdraw timeframe is not active");

        uint256 toBill = getSecondsToBill(msg.sender, _name, secondsToHost);

        require(toBill > 0, "You cannot bill anything right now");

        secondsBilled[msg.sender][_name] = secondsBilled[msg.sender][_name].add(toBill);

        agentsData[_name].intData[1] = agentsData[_name].intData[1].sub(toBill.mul(pricePerSecond));

        msg.sender.transfer(toBill.mul(pricePerSecond));

        emit GotPaid(msg.sender, _name, _proof, timeStartedHostingAgent, secondsToHost,
                     pricePerSecond);

        return (toBill.mul(pricePerSecond), secondsBilled[msg.sender][_name], secondsToHost, agentsData[_name].intData[2]);

    }

    //GETTERS

    function getSecondsToBill(address host, string memory _agent, uint256 secondsToHost) public view returns (uint256) {

        uint256 auxPresent = now;

        uint256 secondsHosted = hostManage.getSecondsHosted(host, _agent);

        bool hostIsActive = hostManage.hostIsActive(host);

        uint256 startOfWork = hostManage.getStartOfWork(host);

        //BUG: the agent might have started being hosted when the host has been online for a while

        if (hostIsActive) secondsHosted = secondsHosted.add(auxPresent - startOfWork);

        if (secondsToHost < secondsHosted) secondsHosted = secondsToHost;

        if (secondsBilled[msg.sender][_agent] >= secondsHosted) return uint256(0);

        return secondsHosted - secondsBilled[msg.sender][_agent];

    }

    function getAgentManagementAddr() public view returns (address) {

        return address(agentManage);

    }

    function getPaymentWindow() public view returns (uint256) {

        return paymentWindow;

    }

    function agentAlreadyRegistered(string memory _name) public view returns (bool) {

        return (agentsData[_name].root.timestamp > 0 && agentsData[_name].root.hostsNumber > 0);

    }

    function getAgentIntData(string memory _name)
      public view returns (uint256, uint256, uint256, uint256) {

        return (agentsData[_name].intData[0], agentsData[_name].intData[1],
                agentsData[_name].intData[2], agentsData[_name].intData[3]);

    }

    function getEarnedFunds(address agent) public view returns (uint256) {

        return earnedFunds[agent];

    }

    function getAgentRoot(string memory _name)
      public view returns (uint256, bytes memory, string memory, uint256) {

        return (agentsData[_name].root.timestamp, agentsData[_name].root._root,
                agentsData[_name].root.ipfsProof, agentsData[_name].root.hostsNumber);

    }

    function agentIsRegistered(string memory _name) public view returns (bool) {

        if (agentsData[_name].root.hostsNumber > 0) return true;
        return false;

    }

    function getSecondsBilled(address host, string memory _name)
      public view returns (uint256) {

        return secondsBilled[host][_name];

    }

    function getContribution(address agent, string memory _name)
      public view returns (uint256) {

        return contributions[agent][_name];

    }

}
