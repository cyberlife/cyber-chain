pragma solidity 0.5.0;

import "contracts/interfaces/IStaking.sol";
import "contracts/interfaces/IHostManagement.sol";
import "contracts/interfaces/IAgentsTreasury.sol";
import "contracts/zeppelin/SafeMath.sol";

contract HostManagement is IHostManagement {

  //TODO: delete hosts data when they unstake

    using SafeMath for uint256;

    struct Host {

        bytes32 hostID;

        uint256 rentPerSecond;

        uint256 agentCount;

        uint256 timeActive;

        bool active;

    }

    struct AgentsTree {

        string ipfsProof;

        bytes32 root;

        uint256 agentsNumber;

    }

    uint256 time_between_price_changes;
    uint256 minimumShutdownTime;

    uint256 lowerBoundRent;

    IStaking stakingContract;
    IAgentsTreasury treasury;

    mapping(bytes32 => address) hostIDtoAddr;
    mapping(address => Host) hostData;
    mapping(address => uint256) nextPriceChange;
    mapping(address => uint256) nextShutdownPermission;
    mapping(address => AgentsTree) hostedAgents;
    mapping(address => mapping(string => uint256)) secondsHosted;
    mapping(address => uint256) startedToWork;

    modifier onlyAfterWait {

        require(nextPriceChange[msg.sender] < now || nextPriceChange[msg.sender] == 0);
        _;

    }

    modifier onlyTreasury {

        require(msg.sender == address(treasury), "The sender is not the treasury");
        _;

    }

    constructor(address _stakingContract, address _treasury) public {

        stakingContract = IStaking(_stakingContract);

        treasury = IAgentsTreasury(_treasury);

    }

    function setTreasury(address _treasury) public onlyOwner {

        require(_treasury != address(0), "The treasury param is null");

        treasury = IAgentsTreasury(_treasury);

    }

    function setLowerBound(uint256 _lowerBound) public onlyOwner {

        require(_lowerBound > 0, "The lower bound must be greater than zero");

        lowerBoundRent = _lowerBound;

        emit SetLowerRentBound(_lowerBound);

    }

    function changeTimeBetweenChanges(uint256 newTime) public onlyOwner {

        time_between_price_changes = newTime;

        emit ChangedTimeBetweenChanged(newTime);

    }

    function changeMinimumShutdownTime(uint256 newTime) public onlyOwner {

        minimumShutdownTime = newTime;

        emit ChangedMinimumShutdownTime(newTime);

    }

    function changeRent(uint256 newPrice) public onlyAfterWait {

        require(lowerBoundRent <= newPrice, "The price must be at least lowerBoundRent");

        require(stakingContract.getAmountStaked(msg.sender) > 0, "The host must be staked");

        hostData[msg.sender].rentPerSecond = newPrice;

        nextPriceChange[msg.sender] = now.add(time_between_price_changes * 1 seconds);

        emit ChangedRent(msg.sender, newPrice);

    }

    function setID(string memory ID) public {

        bytes32 auxID = stringToBytes32(ID);

        require(stakingContract.getAmountStaked(msg.sender) > 0, "The host must be staked");
      //  require(utfStringLength(ID) == 46, "The ID must have 46 characters");

        hostData[msg.sender].hostID = auxID;
        hostIDtoAddr[auxID] = msg.sender;

        emit SetID(msg.sender, auxID);

    }

    function setAgentsTree(address host, string memory ipfs,
      bytes32 root, uint256 agentsNumber) public {

        AgentsTree memory tree = AgentsTree(ipfs, root, agentsNumber);

        hostedAgents[host] = tree;

        emit ChangedAgentsTree(host, root, agentsNumber);

    }

    function broadcastActivity() public {

        require(stakingContract.getAmountStaked(msg.sender) > 0, "The host must be staked");

        require(hostData[msg.sender].hostID.length > 0, "The host id must be speficied");
        require(hostData[msg.sender].rentPerSecond > 0, "The host must have a rent specified");

        hostData[msg.sender].active = true;

        startedToWork[msg.sender] = now;

        nextShutdownPermission[msg.sender] = startedToWork[msg.sender].add(minimumShutdownTime * 1 seconds);

        emit BroadcastedActivity(msg.sender);

    }

    function shutdown() public {

        require(nextShutdownPermission[msg.sender] > 0 && now >= nextShutdownPermission[msg.sender],
            "You must wait until the nextShutdownPermission[msg.sender] timestamp");

        hostData[msg.sender].active = false;

        startedToWork[msg.sender] = 0;

        nextShutdownPermission[msg.sender] = 0;

        emit Shutdown(msg.sender);

    }

    function registerSeconds(address host, string memory agent, uint256 secNumber) public {

        secondsHosted[host][agent] = secondsHosted[host][agent].add(secNumber);

        emit RegisteredSeconds(host, agent, secNumber);

    }

    //GETTER

    function stringToBytes32(string memory source) public view returns (bytes32) {

        bytes memory tempEmptyStringTest = bytes(source);

	bytes32 result;

        if (tempEmptyStringTest.length == 0) {

            return 0x0;

        }

        assembly {

            result := mload(add(source, 32))

        }

	return result;

    }

    function canHostAgents(bytes32 id) public view returns (bool) {

        address hostAddr = hostIDtoAddr[id];

        return (

            bytes32('') != hostData[hostAddr].hostID
            && hostData[hostAddr].rentPerSecond > 0
            && hostData[hostAddr].active == true
            && stakingContract.getAmountStaked(hostAddr) >=
               stakingContract.getStakeNeeded()
            //&& stakingContract.getWithdrawalTime(hostAddr) - (block.timestamp) > stakingContract.getAcceptanceTillWithdraw()

          );
      //      && stakes.getStartTime(hostAddr) < now

    }

    //Added because of stupid and buggy web3js 1.0 implementation that always throws an erorr in cyber-client

    function stringCanHostAgents(string memory id) public view returns (bool) {

      bytes32 convertedId = stringToBytes32(id);

      address hostAddr = hostIDtoAddr[convertedId];

      return (

          bytes32('') != hostData[hostAddr].hostID
          && hostData[hostAddr].rentPerSecond > 0
          && hostData[hostAddr].active == true
          && stakingContract.getAmountStaked(hostAddr) >=
             stakingContract.getStakeNeeded()

        );

    }

    function hostPrice(string memory id) public view returns (uint256) {

        bytes32 auxID = stringToBytes32(id);

        address hostAddr = hostIDtoAddr[auxID];

        return hostData[hostAddr].rentPerSecond;

    }

    function priceFromAddress(address id) public view returns (uint256) {

        return hostData[id].rentPerSecond;

    }

    function hostIDfromAddr(address host) public view returns (bytes32) {

        return hostData[host].hostID;

    }

    function hostAddressfromID(string memory id) public view returns (address) {

        bytes32 auxID = stringToBytes32(id);

        return hostIDtoAddr[auxID];

    }

    function getAgentsNumber(string memory id) public view returns (uint256) {

        bytes32 auxID = stringToBytes32(id);

        address hostAddr = hostIDtoAddr[auxID];

        return hostedAgents[hostAddr].agentsNumber;

    }

    function getAgentsTree(address host) public view returns (string memory, bytes32) {

        return (hostedAgents[host].ipfsProof, hostedAgents[host].root);

    }

    function getAgentsRoot(address host) public view returns (bytes32) {

      return hostedAgents[host].root;

    }

    function getTimeBetweenChanges() public view returns (uint256) {

        return time_between_price_changes;

    }

    function getMinimumShutdownTime() public view returns (uint256) {

        return minimumShutdownTime;

    }

    function getShutdownPermission(address host) public view returns (uint256) {

        return nextShutdownPermission[host];

    }

    function hostIsActive(address host) public view returns (bool) {

        return hostData[host].active;

    }

    function getSecondsHosted(address host, string memory agent) public view returns (uint256) {

        return secondsHosted[host][agent];

    }

    function getStartOfWork(address host) public view returns (uint256) {

        return startedToWork[host];

    }

    //UTILS

    function compareStrings(string memory a, string memory b) public view returns (bool){
       return keccak256(abi.encodePacked(a)) == keccak256(abi.encodePacked(b));
    }

}
