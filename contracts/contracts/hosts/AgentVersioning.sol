pragma solidity 0.5.0;

import "contracts/interfaces/IAgentManagement.sol";
import "contracts/interfaces/IStaking.sol";
import "contracts/interfaces/IAgentVersioning.sol";

contract AgentVersioning is IAgentVersioning {

  mapping(address => mapping(string => uint256)) agentVersions;

  address owner;

  IAgentManagement agentManagement;
  IStaking staking;

  modifier onlyStakedHost() {

    require(staking.getIsCurrentlyStaked(msg.sender) == true, "The sender does not have a stake");

    _;

  }

  modifier onlyOwner() {

    require(msg.sender == owner, "The sender is not the owner");

    _;

  }

  constructor(address management, address _staking) public {

    owner = msg.sender;

    agentManagement = IAgentManagement(management);

    staking = IStaking(_staking);

  }

  function changeOwner(address _newOwner) public onlyOwner {

    owner = _newOwner;

  }

  function setManagement(address _management) public onlyOwner {

    require(_management != address(0), "The management param is null");

    agentManagement = IAgentManagement(_management);

  }

  function setStaking(address _staking) public onlyOwner {

    require(_staking != address(0), "The staking address param is null");

    staking = IStaking(_staking);

  }

  //Changed name

  function updateAgentVersion(string memory _agent) public onlyStakedHost {

    address agentCreator = agentManagement.getCreatorFromName(_agent);
    uint256 position = agentManagement.getPositionFromName(_agent);

    uint256 version = agentManagement.getAgentVersion(agentCreator, position);

    agentVersions[msg.sender][_agent] = version;

    emit ChangedAgentVersion(msg.sender, _agent);

  }

  //Changed name

  function clearAgent(string memory _agent) public onlyStakedHost {

    agentVersions[msg.sender][_agent] = 0;

    emit ClearedAgent(msg.sender, _agent);

  }

  //GETTERS

  //Changed name

  function getHostAgentVersion(address host, string memory agent)
    public view returns (uint256) {

    return agentVersions[host][agent];

  }

}
