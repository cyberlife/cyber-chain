pragma solidity 0.5.0;

import "contracts/zeppelin/SafeMath.sol";
import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IHostManagement.sol";
import "contracts/interfaces/IAgentManagement.sol";
import "contracts/interfaces/IAssigningJobs.sol";

contract AssigningJobs is Ownable, IAssigningJobs {

  using SafeMath for uint256;

  mapping(string => bool) changingTree;

  mapping(string => address) hostsChangingTrees;

  mapping(address => bool) isChangingTree;

  IHostManagement hostManagement;
  IAgentManagement agentManagement;

  address treasury;

  modifier onlyHostManagement() {

    require(msg.sender == address(hostManagement), "The sender is not the host management contract");
    _;

  }

  modifier onlyTreasury {

    require(msg.sender == treasury, "The sender is not the treasury");
    _;

  }

  modifier canInteract(address host) {

    bytes32 id = hostManagement.hostIDfromAddr(host);

    require(hostManagement.canHostAgents(id) == true, "The host cannot interact with agents right now");

    _;

  }

  modifier hostOrCreator(string memory agent) {

    address creatorAddr = agentManagement.getCreatorFromName(agent);

    uint256 position = agentManagement.getPositionFromName(agent);

    bool creatorIsAgent = agentManagement.getCreatorIsAgent(creatorAddr, position);

    if (creatorIsAgent) {

      //[PRODUCTION] check if the agent creator is allowed to do this in case it called; else check host validity

      _;

    } else {

      bytes32 id = hostManagement.hostIDfromAddr(msg.sender);

      bool autonomy = agentManagement.getAgentAutonomy(creatorAddr, position);

      if (autonomy) {

        require(msg.sender != creatorAddr && hostManagement.canHostAgents(id) == true, "The host is not authorised to call this function or the agent is autonomous and the creator called");

      } else {

        require(msg.sender == creatorAddr || hostManagement.canHostAgents(id) == true, "The host is not authorised to call this function or the caller is not a host and is not the creator");

      }

    }

    _;

  }

  constructor(address _management, address agentMan, address _treasury) public {

    hostManagement = IHostManagement(_management);

    agentManagement = IAgentManagement(agentMan);

    treasury = _treasury;

  }

  function setHostManagement(address _management) public onlyOwner {

    hostManagement = IHostManagement(_management);

  }

  function setAgentManagement(address _management) public onlyOwner {

    agentManagement = IAgentManagement(_management);

  }

  function setTreasury(address _treasury) public onlyOwner {

    treasury = _treasury;

  }

  //Changed to agent

  function changingAgentTree(string memory agent) public hostOrCreator(agent) {

    require(changingTree[agent] == false, "The agent is already being migrated");

    changingTree[agent] = true;
    hostsChangingTrees[agent] = msg.sender;
    isChangingTree[msg.sender] = true;

  }

  function finishedChangingTree(string memory agent) public hostOrCreator(agent) {

    require(changingTree[agent] == true, "The agent is not being migrated");
    require(msg.sender == hostsChangingTrees[agent], "This address did not initialize the agent migration");

    changingTree[agent] = false;
    hostsChangingTrees[agent] = address(0);
    isChangingTree[msg.sender] = false;

  }

  //GETTERS

  function getChangingTree(string memory agent) public view returns (bool) {

    return changingTree[agent];

  }

  function getIsChangingTree(address host) public view returns (bool) {

    return isChangingTree[host];

  }

  function getHostChangingTree(string memory agent) public view returns (address) {

    return hostsChangingTrees[agent];

  }

}
